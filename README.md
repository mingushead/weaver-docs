# weaver-docs
Documentation site for WeaverPlatform.

run:
```
$ npm i
$ npm start
```

get a deployable bundle:
```
$ npm build
```