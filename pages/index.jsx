import React from 'react'
import DocumentTitle from 'react-document-title'
import { Link } from 'react-router'
import { Container, Grid, Span } from 'react-responsive-grid'
import { prefixLink } from 'gatsby-helpers'
import includes from 'underscore.string/include'
import { colors, activeColors } from 'utils/colors'
import { init, start, stop } from 'pages/anim/anim'

import typography from 'utils/typography'
import { config } from 'config'

exports.data = {
  title: 'Home',
}

const homepage = React.createClass({
  componentDidMount: function() {
    init();
    start();
  },
  render () {
    const page = this.props.route.page

    return (
        <DocumentTitle title={`${page.data.title} | ${config.siteTitle}`}>
          <div>
            <div id="container"></div>
            <span className="main-header">Weaver<b>Platform</b></span>
            <div className="docs-jumbotron">
              <img src="img/docs-jumbotron.png"/>
            </div>
            <span className="main-subtitle">Graph database management.</span>
            <div className="button-container">
              <Link
                  className="disabled"
                  style={{
                        background: '#006B75',
                        opacity:0.2
                      }}
                  >
                users
              </Link>
              <Link
                  to={prefixLink('/developers/')}
                  style={{
                background: '#00ACC1'
              }}
                  >
                developers
              </Link>
            </div>
          </div>
        </DocumentTitle>
    )
  },
})


export default homepage
