import React from 'react'
import { Link } from 'react-router'
import Breakpoint from 'components/Breakpoint'
import find from 'lodash/find'
import { prefixLink } from 'gatsby-helpers'
import { config } from 'config'

import typography from 'utils/typography'
const { rhythm } = typography

module.exports = React.createClass({
  propTypes () {
    return {
      route: React.PropTypes.object,
    }
  },
  contextTypes: {
    router: React.PropTypes.object.isRequired,
  },
  handleTopicChange (e) {
    return this.context.router.push(e.target.value)
  },

  render () {
    const childPages = config.userPages.map((p) => {
      const page = find(this.props.route.pages, (_p) => _p.path === p)
      return {
        title: page.data.title,
        path: page.path,
      }
    })
    const docOptions = childPages.map((child) =>
      <option
          key={prefixLink(child.path)}
        value={prefixLink(child.path)}
      >
        {child.title}
      </option>

    )
    const apps = config.appGuidePages.map((p) => {
      const page = find(this.props.route.pages, (_p) => _p.path === p)
      return {
        title: page.data.title,
        path: page.path,
      }
    })
    const appList = apps.map((child) => {
      const isActive = prefixLink(child.path) === this.props.location.pathname
      return(
          <li key={child.path} className='modules-item'>
            <Link
                to={prefixLink(child.path)}
                >
              {isActive ? <strong>{child.title}</strong> : child.title}
            </Link>
          </li>
      )
    })
    const devPages = childPages.map((child) => {
      const isActive = prefixLink(child.path) === this.props.location.pathname
      const appsActive = prefixLink(child.path).indexOf("/users/apps/") !== -1
      const isRefPages = this.props.location.pathname.indexOf("/users/apps/") !== -1
      return (
        <li
          key={child.path}
          style={{
            //borderBottom: '1px solid ' + config.baseColor,
            margin: `${rhythm(1/2)} ${rhythm(2)}`,
            padding: `${rhythm(1/8)} 0`
          }}
        >
          <Link
            to={prefixLink(child.path)}
            style={{
              textDecoration: 'none',
            }}
          >
            {isActive ? <strong>{child.title}</strong> : child.title}
          </Link>
          <ul>
            {isRefPages && (isActive || appsActive) ? appList : ''}
          </ul>
        </li>
      )
    })
    return (
      <div>
        <Breakpoint
          mobile
        >
          <div className="left-hand-bar">
            <ul
              style={{
                marginTop: `${rhythm(1/2)}`,
                listStyle: 'none',
                margin: '0'
              }}
            >
              {devPages}
            </ul>
          </div>
          <div
            style={{
              marginLeft: '220px',
              padding: `${rhythm(1)}`,
              paddingTop: rhythm(3)
            }}
          >
            {this.props.children}
          </div>

        </Breakpoint>
        <Breakpoint>
          <strong>Topics:</strong>
          {' '}
          <select
            defaultValue={this.props.location.pathname}
            onChange={this.handleTopicChange}
          >
            {docOptions}
          </select>
          <br />
          <br />
          {this.props.children}
        </Breakpoint>
      </div>
    )
  },
})
