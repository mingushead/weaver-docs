---
title: Setup
---

## Black-box installation (front-end devs)

###### Required installations:
- Docker

#### Installation

First, clone the [sdk](https://github.com/weaverplatform/weaver-sdk-js) somewhere.
```
$ git clone https://github.com/weaverplatform/weaver-sdk-js.git
```
Within the cloned sdk, you will find a folder which contains everything you need to set up a test-server.
Run docker-compose while inside this folder
```
$ cd weaver-sdk-js/test-server
$ docker-compose up
```
That's it! Require a weaver-sdk in a client somewhere, and point it to http://localhost:9487
to begin interacting with your newly acquired weaver instance.

```javascript
var weaver = require('weaver-sdk-js');
weaver.connect('http://localhost:9487').then(function(){
    //start weaving!
})
```

## Installation from source (back-end devs)
###### Required installations:
- Docker

#### Installation:

You will need to clone or download a copy of the [weaver-server source code](https://github.com/weaverplatform/weaver-server).

```
$ git clone https://github.com/weaverplatform/weaver-server.git
$ cd weaver-server
$ npm i
```

Then, run all the dependant modules via docker
```
$ docker-compose up
```

Now boot up the server
```
$ node_modules/coffee-script/bin/coffee src/
```

 ### Test everything's working

 If you navigate to the location your running weaver-server is exposed
 (default is localhost:9487), you should see a Weaver icon against a black backdrop.

 ### Get your hands dirty

 You can now interact with your running weaver-server, either with a client using one of our sdks,

```javascript
var weaver = require('weaver-sdk-js');
weaver.connect('http://localhost:9487').then(function(){
    //start weaving!
})
```
or with the weaver-server REST api.
```
$ curl http://localhost:9487/application/version
$ 2.1.4
$
```
