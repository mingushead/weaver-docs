---
title: Some-examples
---

#### Stock checker
Make a grocery-shop stock checker, with different dbs for different shops.

#### To-do list.
Make a todo list, with authentication, and group tasks. (link profiles etc.)

#### Decisions app
Decisions app (social-networking, user privileges etc.)

#### And if you're stuck, try these things

1. Check out the FAQ section.
2. Ask us something on slack
3. Report a git issue
4. Go ahead and change some code and make a pr if you see a shortcoming

