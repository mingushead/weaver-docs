import React from 'react'
import { Link } from 'react-router'
import Breakpoint from 'components/Breakpoint'
import find from 'lodash/find'
import { prefixLink } from 'gatsby-helpers'
import { config } from 'config'
import { stop } from 'pages/anim/anim'

import typography from 'utils/typography'
const { rhythm } = typography

module.exports = React.createClass({
  propTypes () {
    return {
      route: React.PropTypes.object,
    }
  },
  contextTypes: {
    router: React.PropTypes.object.isRequired,
  },
  handleTopicChange (e) {
    return this.context.router.push(e.target.value)
  },
  componentDidMount: function() {
    stop();
    window.addEventListener('scroll', function() {
      if (window.pageYOffset < 69) {
        document.getElementById('side-menu-container').style.marginTop = '-' +window.pageYOffset + 'px';
      }
    });
  },

  render () {
    const childPages = config.devPages.map((p) => {
      const page = find(this.props.route.pages, (_p) => _p.path === p)
      return {
        title: page.data.title,
        path: page.path,
      }
    })
    const docOptions = childPages.map((child) =>
      <option
          key={prefixLink(child.path)}
        value={prefixLink(child.path)}
      >
        {child.title}
      </option>

    )
    const modules = config.devReferencePages.map((p) => {
      const page = find(this.props.route.pages, (_p) => _p.path === p)
      return {
        title: page.data.title,
        path: page.path,
      }
    })
    const moduleList = modules.map((child) => {
      const isActive = prefixLink(child.path) === this.props.location.pathname
      return(
          <li key={child.path} className='modules-item'>
            <Link
                to={prefixLink(child.path)}
                >
              {isActive ? <strong>{child.title}</strong> : child.title}
            </Link>
          </li>
        )
      })

    const docPages = childPages.map((child) => {
      const isActive = prefixLink(child.path) === this.props.location.pathname
      const modulesActive = prefixLink(child.path).indexOf("/developers/reference/") !== -1
      const isRefPages = this.props.location.pathname.indexOf("/developers/reference/") !== -1
      return (
        <li
          key={child.path}
          style={{
            //borderBottom: '1px solid ' + config.baseColor,
            margin: `${rhythm(1/16)} ${rhythm(2)}`,
            padding: `${rhythm(1/8)} 0`,
          }}
        >
          <Link
            to={prefixLink(child.path)}
            style={{
              textDecoration: 'none',
            }}
          >
            {isActive ? <strong>{child.title}</strong> : child.title}
          </Link>
          <ul>
            {isRefPages && (isActive || modulesActive) ? moduleList : ''}
          </ul>
        </li>
      )
    })
    return (
      <div>
        <Breakpoint
          mobile
        >
          <div className="left-hand-bar">
            <ul
              id="side-menu-container"
              style={{
                marginTop: `${rhythm(1/2)}`,
                listStyle: 'none',
                margin: '0'
              }}
            >
              {docPages}
            </ul>
          </div>
          <div
            style={{
              marginLeft: '220px',
              padding: `${rhythm(1)}`,
              paddingTop: rhythm(3)
            }}
          >
            {this.props.children}
          </div>

        </Breakpoint>
        <Breakpoint>
          <strong>Topics:</strong>

          <select
            defaultValue={this.props.location.pathname}
            onChange={this.handleTopicChange}
          >
            {docOptions}
          </select>


          <br />
          <br />
          {this.props.children}
        </Breakpoint>
      </div>
    )
  },
})
