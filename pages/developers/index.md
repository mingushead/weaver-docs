---
title: Introduction
---

A minimal Weaver deployment consists of a running weaver-server,
which is connected to one or more databases. On top of this there are options for a range of 
micro-services, as well as an ever-expanding set of WeaverApps
(both dev-centred and user-centred), with which to interact more
efficiently with the sdk. A flow chart describing the complete set of
currently available components can be found [here](https://github.com/weaverplatform/weaverplatform).
