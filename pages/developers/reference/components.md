---
title: Weaver-components
---

<div align="center">
  <p>
    <img width="350px" src="/img/icon.png" alt="weaver" />
  </p>
</div>

Some configurable and reusable components to use in weaver apps

- **[weaver-listview](#listview)**
  - **[Usage](#listview-usage)**
  - **[API Documentation](#listview-api)**
    - onInit
    - queryService
    - nodeParser
    - headers
    - filters
    - bulkStateName
    - defaultSorter
    - click
    - id
    - layoutFill
<!-- - **[weaver-treeview](#treeview)**
  - config
  - example -->


<a id="listview"></a>
## weaver-listview
<a id="listview-usage"></a>
### usage
Here's a sample controller:

```coffee
ListViewController = ($state)->
  "ngInject"

  vm = @

  vm.exposeProperties = (node)->
    node.name        = node.get('name')
    node.father      = node.relation('hasFather').first().get('name')
    node.grandFather = node.relation('hasFather').first()?.relation('hasFather').first().get('name')
    node

  vm.getQuery = ->
    new Weaver.Query()
    .hasRelationOut('type', Weaver.Node.get('type:Human'))
    .selectRecursiveOut('hasFather')

  vm.headers = [
    { key: "name", label: 'Name' },
    { key: "nodeId", label: 'Weaver ID', enabled: false }, # {enabled:false} means it is not shown by default, but can still have it's visibility toggled by the user
    {
      key: "father"
      label: "Father"
      filter: {
        name:"father"
        label: "Father"
        type:"text"
        filterQuery: (q, val)->
          q.hasRelationOut('hasFather',
            new Weaver.Query().contains('name', val)
          ) if val
          q
      }
    },
    {
      key:   "grandFather"
      label: "Grandfather"
      filter: {
        name:  "grandFather"
        label: "Grandfather"
        type:  "picklist"
        filterQuery: (q, val)->
          q.hasRelationOut('hasFather',
            new Weaver.Query().hasRelationOut('hasFather', val)
          ) if val
          q
        opts:
          query:->
            new Weaver.Query().hasRelationIn('hasFather', new Weaver.Query().hasRelationIn('hasFather'))
          filter:(q,val)->
            q.contains('name',val) if val
            q
          parser:(gFather)->
            gFather
      }
    },
  ]
  # These are the extra filters to go in the component header, they are typically not associated with any column in the table
  vm.filters = [
    {
      name:"noKids",
      label: 'Does not have children',
      type:"checkbox",
      filterQuery: (q,val)->
        if val
          q.hasNoRelationIn('hasFather')
        else
          q
    },
    {
      name:"noSibs",
      label: 'Does not have siblings',
      type:"checkbox",
      filterQuery: (q,val)->
        if val
          q.hasNoRelationOut('hasSibling')
        else
          q
    }
  ]

  vm.nameFilter =
    name:"name",
    label: 'Name'
    type:"text",
    key:"name", # used for sorting
    filterQuery: (q,val)->
      q.contains('name',val) if val
      q

  vm.onClick = (node)->
    $state.go('app.listView.detail',{id:node.id()})
)
```
And an associated template:

```html
<weaver-listview
  node-parser="vm.exposeProperties"
  query-service="vm.getQuery"
  headers="vm.headers"
  filters="vm.filters"
  name-filter="vm.nameFilter"
  default-sorter="'name'"
  click="vm.onClick"
  id="list-view-container">
</weaver-listview>
```
<a id="listview-api"></a>
### API
#### onInit:
Called after the list-view has been initialised

#### queryService:
Should return a default query to populate the list (without filtering, sorting, etc.)

#### nodeParser:
Should prepare `Weaver.Node`s for exposition in a list. The list populates cells with object properties on a Node, so if you want to show a node's name in a cell, do something like `node.name = node.get('name')` in the parser, so that you can later define a _Name_ column in `vm.headers`, which exposes the value that the row in question has for the key `name`

#### headers:
An array of objects, each header is used to populate one header cell, and the column below it.
```coffee
  ###
    REQUIRED FIELDS
  ###
  key: "name",   # the key you used to parse this piece of data inside your nodeParser function
  label: 'Name', # the key to display at the table header

  ###
    OPTIONAL FIELDS
  ###
  dbKey: 'name'  # the database key for this piece of data (if it is an attribute). Used for sorting. Don't include this if this piece of data is not an attribute on the node/row
  enabled: true  # whether this column is visible by default, or needs to be toggled by the user
  filter: Filter  # If this is supplied, this header can be filtered inline by the user. The filter should be in the same format as the filter object passed to <filters>
```

#### filters
An array of filters, to be included above the table, for non-column specific filters.

There are 3 types of filters. Here are the common filter properties:
```coffee
  name:"hasChildren"            # name for storing the filter value on a filter object, for combining multiple filters. Make these unique.
  label: 'Has Children'         # text to display on the ui.
  type:"checkbox"               # input type (3 types currently available)
  filterQuery: hasChildrenFilter # QueryFragment to include in the query for filling the table. Format shown below:
```

##### QueryFragment
```coffee
# This query fragment will be added to the main query
hasChildFilter = (q, val)-> # takes two args
    # val is obtained from vm.filterObject[filter.name], this is the reason for ensuring that the name field is unique for each filter.
    q.hasRelationOut('PhysicalObject.consistsOf') if val # val will be true if the checkbox is ticked
```
##### Filter Types
###### Checkbox
Takes only default filter parameters
###### Text
Takes only default parameters. Here is a sample:
```coffee
  name:"note"
  label: "Note"
  type:"text"
  filterQuery: (q, val)->
    q.contains('PhysicalObject.note', val) if val
```
###### Picklist
More complex.
###### Static picklist
Used where the values will be supplied during instantiation
```coffee
  name:"status"
  label:"Status"
  key:"status"
  filterQuery: statusFilter
  type:"picklist"
  opts:
    vals: []     # the list of possible values for this picklist
    parser:(s)-> # which part of this node to display as the text for this option in the picklist. If a node is returned from this parser, node.get('name') will be displayed.
      s.id()
```
###### Dynamic picklist
Used where the possible values will be obtained from a query
```coffee
  name:"otlType"        
  label: "OTL Type"     
  type:"picklist"       
  filterQuery: otlFilter
  opts:    # opts is used to query for the options for this picklist
    query: # query used to obtain values for this picklist
      new Weaver.ModelQuery(model.includes.otl)
      .class(model.otl.OTLPhysicalObject)
    filter: (q,val)-> # filter to apply to show only a subset of values for this picklist (text filtering only)
      q.contains("OTLObject.name", val) if val
    parser: (n)->    # which part of this node to display as the text for this option in the picklist. If a node is returned from this parser, node.get('name') will be displayed.
      n
```
The query for populating picklists is lazy, by the way (ie it happens on user-opens-picklist), so adding complex stuff here won't affect overall page load, only user-opens-picklist load.

##### nameFilter
Filter to be applied when a user types into the big box at the top of the <weaver-listview>.
Sample:
```coffee
nameFilter = (q,val)->
    q.contains('NamedObject.name', val) if val
```

#### bulkStateName
The name of the state which should be navigated to when a user initiates a bulk-edit operation from this list.
Bulk-edit mode is disabled if this is not passed.

#### defaultSorter
The default key to use for sorting results.
example: `default-sorter="'PhysicalObject.name'"`

#### click
Function which will be executed when a user clicks a row. This function takes a single argument, the node corresponding to the row which was clicked.

#### saveHeaders
TODO: save header state upon user reorders header/toggles visibility for a header

#### id="list-view-container"
This is required as-is for some scrolling stuff. Please include/don't change this.

#### layout-fill
Should work with angular material scaffolding, also tested with flex="x"

<!--

<a id="treeview"></a>
## weaver-treeview
Insert documentation here. -->
