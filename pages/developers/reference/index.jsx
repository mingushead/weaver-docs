import React from 'react'
import DocumentTitle from 'react-document-title'
import { config } from 'config'
import { Link } from 'react-router'
import { Container, Grid, Span } from 'react-responsive-grid'
import { prefixLink } from 'gatsby-helpers'

exports.data = {
  title: 'Reference',
}

const ReferenceList = React.createClass({
  render () {
    const page = this.props.route.page
    const childPages = config.devReferencePages.map((p) => {
      const page = this.props.route.pages.find((_p) => _p.path === p)
      return {
        title: page.data.title,
        path: page.path,
      }
    })
    const moduleList = childPages.map((child) =>
      <Link
        className={'reference-links'}
        key={prefixLink(child.path)}
        to={prefixLink(child.path)}
        style={{
          textDecoration: 'none',
        }}
        >
        {child.title}
      </Link>
    )

    return (
        <DocumentTitle title={`${page.data.title} | ${config.siteTitle}`}>
          <div>
            <h1>Reference</h1>
            <p>
              Here's the current stack:
            </p>

            <div className="modules-container">
              {moduleList}
            </div>

          </div>
        </DocumentTitle>
    )
  },
})

export default ReferenceList
