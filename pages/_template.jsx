import React from 'react'
import { Link } from 'react-router'
import { Container, Grid, Span } from 'react-responsive-grid'
import Breakpoint from 'components/Breakpoint'
import { prefixLink } from 'gatsby-helpers'
import includes from 'underscore.string/include'
import { colors, activeColors } from 'utils/colors'

import typography from 'utils/typography'
import { config } from 'config'

// Import styles.
import 'css/main.css'
import 'css/github.css'

const { rhythm, adjustFontSizeTo } = typography

module.exports = React.createClass({
  propTypes () {
    return {
      children: React.PropTypes.object,
    }
  },
  render () {
    const docsActive = includes(this.props.location.pathname, '/developers/')
    const examplesActive = includes(this.props.location.pathname, '/users/')

    return (
      <div>
        <div
          style={{
            background: config.baseColor,
            color: '#fff',
            padding: rhythm(0.33),
            position: 'absolute',
            fontFamily: 'Lato',
            width: '100vw',
            zIndex: '1000'
          }}
        >
          <Container
            style={{
              maxWidth: 960,
              paddingLeft: rhythm(3/4),
            }}
          >
            <Grid
              columns={12}
              style={{
                padding: `${rhythm(3/4)} 0`,
              }}
            >
              <Span
                columns={2}
                style={{
                  height: 24, // Ugly hack. How better to constrain height of div?
                }}
              >
                <img
                    src='/img/logo.png'
                    style={{
                        position: 'absolute',
                        left: '22px',
                        top: '18px',
                        width: '37px',
                        display: 'inline'
                      }}
                ></img>
                <Link
                  to={prefixLink('/')}
                  style={{
                    position: 'absolute',
                    left: '70px',
                    letterSpacing: '0.05rem',
                    textDecoration: 'none',
                    color: colors.fg,
                    fontSize: '1rem',
                    fontWeight: 100
                  }}
                >
                  Weaver<b style={{fontWeight: '500'}}>Docs</b>
                </Link>
              </Span>
              <Span columns={10} last
                  style={{
                    textTransform: 'uppercase',
                    fontWeight: '700',
                    fontSize: '1rem'
                  }}>
                <a
                  style={{
                    float: 'right',
                    color: '#fff',
                    textDecoration: 'none',
                    fontFamily: 'Lato',
                    fontWeight: '500',
                    fontSize: '0.9rem',
                    marginLeft: rhythm(1.5),
                  }}
                  href="https://github.com/weaverplatform"
                >
                  Github
                </a>
                <Link
                  to={prefixLink('/developers/')}
                  style={{
                    background: config.baseColor,
                    color: docsActive ? activeColors.fg : colors.fg,
                    float: 'right',
                    textDecoration: 'none',
                    fontWeight: '500',
                    fontSize: '0.9rem',
                    fontFamily: 'Lato',
                    paddingLeft: rhythm(1.5),
                    paddingRight: rhythm(1/2),
                    paddingBottom: rhythm(3/4),
                    marginBottom: rhythm(-1),
                    paddingTop: rhythm(1),
                    marginTop: rhythm(-1),
                  }}
                >
                  Developers
                </Link>
                <Link
                  style={{
                    background: config.baseColor,
                    color: examplesActive ? activeColors.fg : colors.fg,
                    float: 'right',
                    textDecoration: 'none',
                    fontWeight: '500',
                    fontSize: '0.9rem',
                    fontFamily: 'Lato',
                    opacity: 0.5,
                    paddingLeft: rhythm(1.5),
                    paddingRight: rhythm(1/2),
                    paddingBottom: rhythm(3/4),
                    marginBottom: rhythm(-1),
                    paddingTop: rhythm(1),
                    marginTop: rhythm(-1),
                  }}
                >
                  Users
                </Link>
              </Span>
            </Grid>
          </Container>
        </div>
        <Container
          style={{
            maxWidth: 960,
            padding: `${rhythm(1)}`,
            paddingTop: `${rhythm(4)}`
          }}
        >
          {this.props.children}
        </Container>
      </div>
    )
  },
})
