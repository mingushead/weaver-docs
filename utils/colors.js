import colorPairsPicker from 'color-pairs-picker'
import chroma from 'chroma-js'

import { config } from 'config'

export const colors = colorPairsPicker(config.baseColor, {
  contrast: 10,
})


const darker = chroma(config.baseColor).darken(10).hex()

export const activeColors = {
  fg: chroma('#00B8D4').brighten(5).hex(),
  bg: chroma('#00b8d4').darken(5).hex()
}
